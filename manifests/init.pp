class divaserve (
  $divaserve_php_filename      = $divaserve::params::divaserve_php_filename,
  $divaserve_php_directory     = $divaserve::params::divaserve_php_directory,
  $divaserve_user              = $divaserve::params::divaserve_user,
  $divaserve_group             = $divaserve::params::divaserve_group,
  $divaserve_cache_dir         = $divaserve::params::divaserve_cache_dir,
  $divaserve_image_dir         = $divaserve::params::divaserve_image_dir,
  $divaserve_memcache_server   = $divaserve::params::divaserve_memcache_server,
  $divaserve_memcache_port     = $divaserve::params::divaserve_memcache_port,
  $divaserve_check_restricted  = $divaserve::params::divaserve_check_restricted,
  $divaserve_restricted_url    = $divaserve::params::divaserve_restricted_url,
  $divaserve_restricted_ids    = $divaserve::params::divaserve_restricted_ids,
  $divaserve_enable_disk_cache = $divaserve::params::divaserve_enable_disk_cache,
) inherits divaserve::params {

  file { $divaserve_php_filename:
    ensure  => present,
    owner  => $divaserve_user,
    group  => $divaserve_group,
    seluser => "unconfined_u",
    seltype => "httpd_sys_content_t",
    mode    => '0444',
    path    => "$divaserve_php_directory/$divaserve_php_filename",
    content => template('divaserve/divaserve.php.erb'),
  }

  file {restricted_jpeg2000:
    ensure  => present,
    source  => "puppet:///modules/divaserve/restricted.jp2",
    path     => "$divaserve_image_dir/restricted.jp2",
    owner   => $divaserve_user,
    group   => $divaserve_group,
    seluser => "unconfined_u",
    seltype => "etc_runtime_t",
    mode    => '0444',
  }


  # Ensure the cache dir exists and is writable by php
  # The divaserve script tries to do this but may not have necessary permissions
  # Also, setting the $divaserve_cache_dir under /tmp on RHEL/CENTOS doesn't work
  # most likely due to systemds use of private, per-service, tmp directories
  file { $divaserve_cache_dir:
    ensure  => directory,
    owner  => $divaserve_user,
    group  => $divaserve_group,
    mode    => '0777',
  }

  # allow httpd to use get_file_contents over network
  # (required to get list of restricted pages from parish registers)
  selboolean {'httpd_can_network_connect':
    persistent => true,
    value      => 'on',
  }

  # update this to use the selinux module provided in later versions of puppet
  exec {'chcon divcache':
    command => "chcon -R unconfined_u:object_r:httpd_sys_rw_content_t:s0 $divaserve_cache_dir",
    require => File[$divaserve_cache_dir],
    path    => '/usr/bin/',
    user    => 'root',
  }

}
