class divaserve::params {
  $divaserve_php_filename      = "divaserve.php"
  $divaserve_php_directory     = "/var/www/iipsrv"
  $divaserve_user              = "www-data"
  $divaserve_group             = "www-data"
  $divaserve_cache_dir         = "/usr/local/divacache"
  $divaserve_image_dir         = "/usr/local/contentstore/data/nli"
  $divaserve_memcache_server   = "127.0.0.1"
  $divaserve_memcache_port     = "11211"
  $divaserve_check_restricted  = false
  $divaserve_restricted_url    = "172.0.0.1"
  $divaserve_restricted_ids    = []
  $divaserve_enable_disk_cache = true
  $divaserve_verify_peer       = false
}
